---
title: "Logboek Themaopdracht 9"
author:
- Marcel Zandberg 362261
date: "`r Sys.Date()`"
output: 
    pdf_document:
      toc_depth: 3
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

#License note
Copyright (c) 2018 <Marcel Zandberg>.
Licensed under GPLv3. See gpl.md

#LOGBOEK
 
#Assighnment week 1

##research question and project goals

welke metingen van patienten met diabetes zullen de meeste invloed hebben waarop de patient weer binnen 30 dagen terug zal keren naar het ziekenhuis en zal machine learning gegeven deze metingen kunnen voorspellen welke patien wel en niet zullen terugkeren naar het ziekenhuis.

##Document status of knowledge and nature of the data
Ik heb gekozen voor het project Predicting hospital readmission of 
diabetes patients. 
De bijbehorende data is te vinden op: https://archive.ics.uci.edu/ml/machine-learning-databases/00296/
Dit project heeft 55 atributen. 
beschrijving en informatie van de atributen is te vinden op: https://www.hindawi.com/journals/bmri/2014/781670/tab1/ en te zien in figuur 1
\newpage
Figuur 1: Lijst van functies en hun beschrijvingen van de dataset
```{r, echo=FALSE, out.width='85%'}
knitr::include_graphics('/homes/mazandberg/Screenshot_2018-09-11 Impact of HbA1c Measurement on Hospital Readmission Rates Analysis of 70,000 Clinical Database Patien[...].png')
```

##Exploratory Data Analysis 

De klassen zijn oneven gepresenteerd.
De data ziet er goed uit en zonder outliers.
Ik heb de missende waardes verandert naar NA's
De correlatie tussen de verschillende attributen is niet te berekenen door de verschillende data types en levels, hiervoor heb ik wel een crosstable gebruikt die de items van de levels van de attributen count en tegenoverelkaar zet. ook heb ik een annova en Pearson's Chi-squared test gebruikt die wel de correlatie tussen 2 attributen berekent aan de hand van het resultaat van de p-value.
Ik heb ook de atrributen tegen de readmitted attribuut weergegeven om zo meer informatie te krijgen welke data waar impact op heeft op de resultaten van de readmitted attribuut.
Het attribuut A1Cresult kan grote impact hebben op een patient omdat het grote impact heeft op de glucose control. Dit attribuut heeft ook grote impact op de healthcare van diabetes patienten.

De verkregen resultaten zijn terug te vinden in het script EDA.Rmd

```{r read, echo = FALSE}
filename <- "/homes/mazandberg/bitbucket/themaopdracht-thema-09/RLog_part/data/diabetic_data.csv"
full.data <- read.csv(filename, header = T, sep = ",")
```

###librarys
```{r}
library(LaF)
library(DataExplorer)
library(ggplot2)
library(gmodels)
library(RWeka)
```


#dimensions and structure
```{r}
dim(full.data)
str(full.data)
```

###check missing values
```{r}
full.data[full.data == "?"] <- NA
plot_missing(full.data)
```

###data visualisation
```{r}
readmitted <- 50
plot(as.data.frame(full.data[readmitted]),xlab = "Days", ylab ="Frequency", main = "Frequency readmittend of patients", col = c("red","blue","green"))

plot_bar(full.data)

data <- table(full.data$readmitted,full.data$race)
data2 <- data.frame(data)
colnames(data2) <- c("readmitted","race","frequency")
data2 <- data2[-c(1, 2, 3), ]

ggplot(data2, aes(fill=race, y=frequency, x=readmitted)) +
    geom_bar(position="dodge", stat="identity")


data3 <- table(full.data$readmitted, full.data$A1Cresult)
data4 <- data.frame(data3)
colnames(data4) <- c("readmitted","A1Cresult","frequency")

ggplot(data4, aes(fill=A1Cresult, y=frequency, x=readmitted)) +
    geom_bar(position="dodge", stat="identity")

data5 <- table(full.data$readmitted, full.data$insulin)
data6 <- data.frame(data5)
colnames(data6) <- c("readmitted","insuline","frequency")

ggplot(data6, aes(fill=insuline, y=frequency, x=readmitted)) +
    geom_bar(position="dodge", stat="identity")

data7 <- table(full.data$readmitted, full.data$gender)
data8 <- data.frame(data7)
colnames(data8) <- c("readmitted","gender","frequency")

ggplot(data8, aes(fill=gender, y=frequency, x=readmitted)) +
    geom_bar(position="dodge", stat="identity")

data9 <- table(full.data$readmitted, full.data$diabetesMed)
data10 <- data.frame(data9)
colnames(data10) <- c("readmitted","diabetes.medications","frequency")

ggplot(data10, aes(fill=diabetes.medications, y=frequency, x=readmitted)) +
    geom_bar(position="dodge", stat="identity")
```

###table correlation and information between relaps of patients and the other collums
```{r}
#relatie insuline met readmitted patients
CrossTable(full.data$insulin, full.data$readmitted)


#relatie ras met readmitted patients
CrossTable(full.data$race, full.data$readmitted)


#relatie gender met readmitted patients
CrossTable(full.data$gender, full.data$readmitted)


#relatie tijd in ziekenhuis met readmitted patients
CrossTable(full.data$time_in_hospital, full.data$readmitted)


#relatie gebruik diabetes medicijnen met readmitted patients
CrossTable(full.data$diabetesMed, full.data$readmitted)

#relatie A1Cgehalte met readmitted patients
CrossTable(full.data$A1Cresult,full.data$readmitted) #belangrijk!!!
```
##Creating a clean dataset

Het atrribuut gewicht heeft te veel missende values, namelijk meer dan 96 %. Dit attribuut heb ik uit de data verwijderd. Dit heb ik ook met payercode die heeft ook te veel missende values.
De medicijnen die meer dan 95% van de waardes alleen de waarde "no" hebben heb ik ook uit de data verwijdert die hebben dus significant geen toegevoegde waarde tot het attribuut readmitted en dus ook niet op de andere attributen.
De attributen diag1 tot 3 heeft onleesbare data en kan dus niks mee worden gedaan. Deze attributen zal ik verwijderen.
De geprossede data geeft de attributen weer die de meeste impact kunnen hebben op de data van patienten die weer in relapse gaan.
Het script marcel_zandberg_thema9.Rmd maakt de clean data set die weer word omgezet naar diabetes_useful.arff zodat die kan worden ingelezen in Weka.

```{r}


weight <- 6
payercode <-11
diag <- 19:21
metformin <- 25
nateglinide <- 27
chlorpropamide <- 28
acetohexamide <- 30
tolbutamide <- 33
medicene1 <- 36:41
medicene2 <- 43:47
usefuldata <- full.data[, -c(diag,metformin,nateglinide,chlorpropamide,acetohexamide,tolbutamide,medicene1,medicene2)]
usefuldata[usefuldata == "?"] <- NA
 
plot_missing(usefuldata)
```

#sampledata
```{r}

sampledata <- usefuldata[sample(nrow(usefuldata), 10000), ] 
unknownFile <- usefuldata[sample(nrow(usefuldata), 10000), ] 
unknownFile[,"readmitted"] <- "?"
```

###create weka file
```{r}
write.arff(usefuldata, file = "diabetes_useful.arff")
write.arff(sampledata, file = "sampleData.arff")
write.arff(unknownFile, file = "unknownFile.arff")
```


##Investigation of performance of ML algorithms

```{r}
ml_algs <- read.arff("/homes/mazandberg/bitbucket/themaopdracht-thema-09/RLog_part/data/alg_data.arff")
```

TP (instance belongs to a, classified as a) = x
FP (instance belongs to others, classified as a) = x, onder
FN (instance belongs to a, classified as others) = x, zei
TN (instance belongs to others, classified as others) = Total instance - (TP + FP + FN)

Resultaten van de verschillende algoritmes op de data.

###Zero R

ZeroR predicts class value: NO

Time taken to build model: 0.04 seconds

=== Stratified cross-validation ===
=== Summary ===

Correctly Classified Instances       54864               53.9119 %
Incorrectly Classified Instances     46902               46.0881 %
Kappa statistic                          0     
Mean absolute error                      0.3833
Root mean squared error                  0.4378
Relative absolute error                100      %
Root relative squared error            100      %
Total Number of Instances           101766     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,000    0,000    ?          0,000    ?          ?        0,500     0,112     <30
                 0,000    0,000    ?          0,000    ?          ?        0,500     0,349     >30
                 1,000    1,000    0,539      1,000    0,701      ?        0,500     0,539     NO
Weighted Avg.    0,539    0,539    ?          0,539    ?          ?        0,500     0,425     

=== Confusion Matrix ===

     a     b     c   <-- classified as
     0     0 11357 |     a = <30
     0     0 35545 |     b = >30
     0     0 54864 |     c = NO
     
```{r}
ZeroR <- 1:100

paste("true positifes:",mean(ml_algs[ZeroR,]$Num_true_positives))
paste("true negatifes:",mean(ml_algs[ZeroR,]$Num_true_negatives))
paste("false positifes:",mean(ml_algs[ZeroR,]$Num_false_positives))
paste("false negatives:",mean(ml_algs[ZeroR,]$Num_false_negatives))
paste("Elapsed time:",mean(ml_algs[ZeroR,]$Elapsed_Time_training))
paste("accuracy:",mean(ml_algs[ZeroR,]$Percent_correct))
```

###One R
(65102/101766 instances correct)

Time taken to build model: 0.47 seconds

=== Stratified cross-validation ===
=== Summary ===

Correctly Classified Instances       54200               53.2594 %
Incorrectly Classified Instances     47566               46.7406 %
Kappa statistic                          0.1182
Mean absolute error                      0.3116
Root mean squared error                  0.5582
Relative absolute error                 81.3019 %
Root relative squared error            127.5165 %
Total Number of Instances           101766     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,034    0,013    0,252      0,034    0,061      0,056    0,511     0,116     <30
                 0,403    0,291    0,426      0,403    0,414      0,113    0,556     0,380     >30
                 0,720    0,578    0,593      0,720    0,650      0,148    0,571     0,578     NO
Weighted Avg.    0,533    0,415    0,497      0,533    0,502      0,126    0,559     0,457     

=== Confusion Matrix ===

     a     b     c   <-- classified as
   391  4439  6527 |     a = <30
   618 14326 20601 |     b = >30
   541 14840 39483 |     c = NO
   
```{r}
OneR <- 101:200

paste("true positifes:",mean(ml_algs[OneR,]$Num_true_positives))
paste("true negatifes:",mean(ml_algs[OneR,]$Num_true_negatives))
paste("false positifes:",mean(ml_algs[OneR,]$Num_false_positives))
paste("false negatives:",mean(ml_algs[OneR,]$Num_false_negatives))
paste("Elapsed time:",mean(ml_algs[OneR,]$Elapsed_Time_training))
paste("accuracy:",mean(ml_algs[OneR,]$Percent_correct))
```
   
###NaiveBayes

Time taken to build model: 0.36 seconds

=== Stratified cross-validation ===
=== Summary ===

Correctly Classified Instances       57133               56.1415 %
Incorrectly Classified Instances     44633               43.8585 %
Kappa statistic                          0.1289
Mean absolute error                      0.3236
Root mean squared error                  0.4544
Relative absolute error                 84.4377 %
Root relative squared error            103.8031 %
Total Number of Instances           101766     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,103    0,031    0,295      0,103    0,152      0,117    0,634     0,192     <30
                 0,195    0,106    0,497      0,195    0,280      0,123    0,621     0,447     >30
                 0,894    0,743    0,585      0,894    0,707      0,198    0,661     0,681     NO
Weighted Avg.    0,561    0,441    0,522      0,561    0,496      0,163    0,644     0,545     

=== Confusion Matrix ===

     a     b     c   <-- classified as
  1165  2228  7964 |     a = <30
  1755  6916 26874 |     b = >30
  1033  4779 49052 |     c = NO
  
```{r}
NaiveBayes <- 201:300

paste("true positifes:",mean(ml_algs[NaiveBayes,]$Num_true_positives))
paste("true negatifes:",mean(ml_algs[NaiveBayes,]$Num_true_negatives))
paste("false positifes:",mean(ml_algs[NaiveBayes,]$Num_false_positives))
paste("false negatives:",mean(ml_algs[NaiveBayes,]$Num_false_negatives))
paste("Elapsed time:",mean(ml_algs[NaiveBayes,]$Elapsed_Time_training))
paste("accuracy:",mean(ml_algs[NaiveBayes,]$Percent_correct))
```

###SimpleLogistic

=== Stratified cross-validation ===
=== Summary ===

Correctly Classified Instances       59105               58.0793 %
Incorrectly Classified Instances     42661               41.9207 %
Kappa statistic                          0.1655
Mean absolute error                      0.357 
Root mean squared error                  0.4224
Relative absolute error                 93.1517 %
Root relative squared error             96.4887 %
Total Number of Instances           101766     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,013    0,002    0,404      0,013    0,024      0,055    0,645     0,197     <30
                 0,307    0,159    0,509      0,307    0,383      0,173    0,645     0,474     >30
                 0,876    0,681    0,601      0,876    0,713      0,237    0,678     0,693     NO
Weighted Avg.    0,581    0,423    0,547      0,581    0,521      0,194    0,663     0,561     

=== Confusion Matrix ===

     a     b     c   <-- classified as
   143  3791  7423 |     a = <30
   136 10900 24509 |     b = >30
    75  6727 48062 |     c = NO

```{r}
SimpleLogistic <- 301:400

paste("true positifes:",mean(ml_algs[SimpleLogistic,]$Num_true_positives))
paste("true negatifes:",mean(ml_algs[SimpleLogistic,]$Num_true_negatives))
paste("false positifes:",mean(ml_algs[SimpleLogistic,]$Num_false_positives))
paste("false negatives:",mean(ml_algs[SimpleLogistic,]$Num_false_negatives))
paste("Elapsed time:",mean(ml_algs[SimpleLogistic,]$Elapsed_Time_training))
paste("accuracy:",mean(ml_algs[SimpleLogistic,]$Percent_correct))
```

###IBK

=== Stratified cross-validation ===
=== Summary ===

Correctly Classified Instances       47358               46.5362 %
Incorrectly Classified Instances     54408               53.4638 %
Kappa statistic                          0.0744
Mean absolute error                      0.3564
Root mean squared error                  0.597 
Relative absolute error                 92.9973 %
Root relative squared error            136.3774 %
Total Number of Instances           101766     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,154    0,109    0,151      0,154    0,152      0,045    0,523     0,119     <30
                 0,400    0,337    0,390      0,400    0,395      0,063    0,531     0,368     >30
                 0,572    0,475    0,585      0,572    0,578      0,097    0,549     0,568     NO
Weighted Avg.    0,465    0,386    0,468      0,465    0,467      0,079    0,540     0,448     

=== Confusion Matrix ===

     a     b     c   <-- classified as
  1744  4328  5285 |     a = <30
  4312 14232 17001 |     b = >30
  5503 17979 31382 |     c = NO

```{r}
IBK <- 401:500

paste("true positifes:",mean(ml_algs[IBK,]$Num_true_positives))
paste("true negatifes:",mean(ml_algs[IBK,]$Num_true_negatives))
paste("false positifes:",mean(ml_algs[IBK,]$Num_false_positives))
paste("false negatives:",mean(ml_algs[IBK,]$Num_false_negatives))
paste("Elapsed time:",mean(ml_algs[IBK,]$Elapsed_Time_training))
paste("accuracy:",mean(ml_algs[IBK,]$Percent_correct))
```

###J48

=== Stratified cross-validation ===
=== Summary ===

Correctly Classified Instances       59764               58.7269 %
Incorrectly Classified Instances     42002               41.2731 %
Kappa statistic                          0.2146
Mean absolute error                      0.3485
Root mean squared error                  0.4256
Relative absolute error                 90.9325 %
Root relative squared error             97.2268 %
Total Number of Instances           101766     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,035    0,010    0,311      0,035    0,062      0,071    0,626     0,183     <30
                 0,447    0,241    0,499      0,447    0,471      0,212    0,648     0,463     >30
                 0,793    0,537    0,633      0,793    0,704      0,272    0,682     0,686     NO
Weighted Avg.    0,587    0,375    0,550      0,587    0,551      0,229    0,663     0,552     

=== Confusion Matrix ===

     a     b     c   <-- classified as
   394  4939  6024 |     a = <30
   500 15873 19172 |     b = >30
   371 10996 43497 |     c = NO

```{r}
J48 <- 501:600

paste("true positifes:",mean(ml_algs[J48,]$Num_true_positives))
paste("true negatifes:",mean(ml_algs[J48,]$Num_true_negatives))
paste("false positifes:",mean(ml_algs[J48,]$Num_false_positives))
paste("false negatives:",mean(ml_algs[J48,]$Num_false_negatives))
paste("Elapsed time:",mean(ml_algs[J48,]$Elapsed_Time_training))
paste("accuracy:",mean(ml_algs[J48,]$Percent_correct))
```


###RandomForest


```{r}
RandomForrest <- 601:700

paste("true positifes:",mean(ml_algs[RandomForrest,]$Num_true_positives))
paste("true negatifes:",mean(ml_algs[RandomForrest,]$Num_true_negatives))
paste("false positifes:",mean(ml_algs[RandomForrest,]$Num_false_positives))
paste("false negatives:",mean(ml_algs[RandomForrest,]$Num_false_negatives))
paste("Elapsed time:",mean(ml_algs[RandomForrest,]$Elapsed_Time_training))
paste("accuracy:",mean(ml_algs[RandomForrest,]$Percent_correct))
```

###Experimenter results
Weka heeft veel problemen met de grote van de data. Het bestand heeft namelijk 100000 instances waardoor bepaalde algoritmes zoals SMO zeer lang er over doen om gegevens te verkrijgen. Hierdoor heb ik nog geen resultaten verkregen om de algoritmes met elkaar te kunnen vergelijken. wel heb ik een test gedaan met 1000 instances om zo al een beeld te krijgen van hoe wat het beste algoritme zou kunnen zijn.

Local host opgezet met 6 cores en hyperthreading zodat ik met de explorer de data een stuk sneller kan bekijken. Dit werkt helaas alleen niet voor de experimenter. Ik heb de data nu bijna 2 dagen op de school pc laten runnen en die is bijna klaar om geanalyseerd te kunnen worden.

De data is klaar voor de analyse.
Na het analyseren van de data blijkt dat RandomForrest de beste accuracy heeft met 59.84%, dit is niet heel hoog en is dus niet goed om een juiste prediction te maken. Het heeft ook de beste false positive rate heeft namelijk 0.00, dit is zeer goed want er moet zo goed mogelijk resultaat zijn om zo de beste prdiction te kunnen geven. Het heeft ook beste area under roc heeft namelijk 0.65, Het algoritme is niet heel snel, maar omdat snelheid niet belangrijk is voor mijn onderzoeksvraag zal dit niet heel belangrijk zijn. ik kies er dan ook voor om met dit algoritme verder te gaan.



###attribute selection
De attributen hebben allemaal bijna de zelfde impact op de accuracy van het algoritme. Dit zijn de algoritmes die de meeste impact hebben op het algoritme: 53.9119:gender, 53.9119:age, 53.9119:race, 53.9345:admission source, 53.9021:discharge disposition,  53.9149:medical specialty, 53.9119:time spent in hospital, 53.9119:A1cResult en 53.9119:insuline


###RocCurve


```{r, echo=FALSE, out.width='85%'}
knitr::include_graphics('/homes/mazandberg/bitbucket/themaopdracht-thema-09/RLog_part/data/RocCurve.png')
```


###Java gedeelte
met de gegeven scripts als voorbeeld heb ik die proberen te begrijpen en dat is mij aardig gelukt. ik heb mijn eigen parsecommandline gemaakt en als hulp heb ik daar de implementatie van de demo daarvoor gebruikt. kijkende naar de demo hoe die resultaten genereerde zo heb ik mijn weka api waarvan ik ook deel de implementatie van de demo voor hebt gebruikt aan de argparser geconnect enzo een volledig fail proof en user friendly programma gemaakt die onbekende instance classificieerd aan de hand van bekende data en een model met geoptimaliseerde settings.