# Rlog ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

This is a log where in I wrote what I have done and accomplished so far durig the project.

### Requirements
* Linux, Windows or MAC
* Rstudio
* r for Rstudio


## Files Includes With This Project:
* Project folder: Rlog_part
* contains: scripts, docs and data
* main file is in scripts/marcel_zandberg_thema9.Rmd


## Usage
* Start Rstudio and open the main file in the scripts folder to see code
* look in docs folder for the results in pdf format


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
